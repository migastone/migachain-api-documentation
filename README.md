# **MIGACHAIN - Ethereum Notarization & IPFS Storing Webservice [API Docs]**
### *The easiest way to notarize a document via blockchain and save it forever on the network.*

---

[![Migachain Logo](https://bitbucket.org/migastone/migachain-api-documentation/raw/1dad84cdad04b991455df25d3f6cdf868cd17197/docs_images/logo.png)](https://migachain.com/)

---

Notarization allows you to prove legally and technically, without any doubt, that a particular file existed at a specific time.

The applications of this technology are endless, you can certify that a photo existed at a certain time, or certify the logs of your server forever or validate the existence of a contract in pdf forever.

Our API is very simple, just submit the file you want to notarize, even encrypted, and we answer you with the Ethereum Address where we stored the Hash and the IPFS ID where you can find in the cloud the copy of your file.

The benefits of our API webservice:

![Migachain API Benefits](https://bitbucket.org/migastone/migachain-api-documentation/raw/dc19b6271e7f7d2d5bd741c869b1e73e654a69da/docs_images/benifits.png)

## Technical Details
---

#### API End Point:

```bash
[POST] https://migachain.com/webservice/public/api/filenotary
```

#### List of API parameters and details:

Parameter 			    	| type 			| Description														    
:---------------------- 	| :-----------: | :--------------------------------------------------------------------------------------
**`request[token]`**  	    | string  		| API token that you have received via email or directly from us.
**`request[client_id]`**    | string  		| API client id that you have received via email or directly from us.
**`request[notary_type]`**	| integer 		| Notarization access type which is assigned to you. Possible values are `1 = Hash Notarization`, `2 = File Notarization`, and `3 = Full Notarization`. Default is `3 = Full Notarization`.
**`request[sha256]`**  	    | string  		| Required only when `notary_type = 1` means `Hash Notarization`.
**`request[uploadfile]`**  	| file  		| Required only when `notary_type = 2` or `notary_type = 3` means either `File Notarization` or `Full Notarization`.

#### List of API calls unsuccessful responses and details:

Response 			    																	| Description														
:------------------------------------------------------------- 								| :------------------------------------
`{"response":"Invalid","message":"No data posted."}`  										| You have not posted any data with the API call.
`{"response":"Invalid","message":"Token is missing."}`  									| You have not sent or sent an empty token.
`{"response":"Invalid","message":"Client ID is missing."}`  								| You have not sent or sent an empty client id.
`{"response":"failure","message":"IP is in blocked list."}`  								| Your IP address has been blocked due to some reason.
`{"response":"failure","message":"Same IP address have more than n requests today."}`  		| You have reached the maximum number of requests for a day.
`{"response":"failure","message":"Notarizations monthly limit exceeded."}`  				| Your notarizations monthly limit has been exceeded.
`{"response":"failure","message":"Client ID not valid."}`  									| You have posted an invalid client id.
`{"response":"failure","message":"Client is blocked."}`  									| Your account is blocked by the admin.
`{"response":"failure","message":"No notarization access is assigned."}`  					| Notarization access type is not assigned to you yet.
`{"response":"failure","message":"Requested notarization access is not allowed."}`  		| You are are requesting for a not allocated notarization access type.
`{"response":"failure","message":"SHA256 hash is missing."}`  								| You are using `Hash Notarization` but you have not posted the hash itself.
`{"response":"failure","message":"Valid with In-Active token."}`  							| You token is valid but not active.
`{"response":"failure","message":"Token Invalid."}`  										| You have posted an invalid token.
`{"response":"failure","message":"Your access date is expired. You access date is between (start_date - end_date)."}`  										| Your account validity date is expired.
`{"response":"failure","message":"Daily request exceeds. Your Per day requests are requests_per_day."}`  							| You total number of requests per day are exceeded.
`{"response":"failure","message":"File Missing."}`  										| You are using either `File Notarization` or `Full Notarization` but you have not posted/uploaded the file.
`{"response":"failure","message":"Your file size quota is exceeded from actual size per_file_quota_size Mbs."}`  										| You have uploaded a file greater than the file size quota allocated to you.
`{"response":"failure","message":"Your Life time file size quota is exceeded from actual size lifetime_files_quota_size Mbs."}`  										| You have reached to the maximum file size quota allocated to you forever.
`{"response":"failure","message":"Duplicate call. We have found the same notarization call in our logs. You can see the information in the response."}`  										| You did a duplicate call for same resource again.

#### A successful API call using PHP (cURL):

```php
<?php

$curl = curl_init();

curl_setopt_array($curl, [
  CURLOPT_URL => "https://migachain.com/webservice/public/api/filenotary",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => [
	  'request[token]' => 'YOUR_TOKEN',
	  'request[client_id]' => 'YOUR_CLIENT_ID',
	  'request[uploadfile]'=> new CURLFILE('path/to/file/file.xml'),
	  'request[notary_type]' => '3' // Full Notarization
	],
]);

$response = curl_exec($curl);

curl_close($curl);
echo $response;

```

#### A successful API response (JSON):

```javascript

{
    "response": "success",
    "message": "IPFS Call Successful. Ethereum Call Successful.",
    "eth_address": "0xc72f78c9ebd0e843e61ec40d247f2cc504441fc3",
    "ipfs_address": "https://ipfs.io/ipfs/QmTc8d3W3WHaJ7Y6XfEgaYVLqvoEa9YuNa155nCVz6xH8w",
    "eth_sha_hash": "0caec2cfdf4c36fc65508198933a5d3dc0da4e243acc99f0c3ec977d5a4b652f",
    "eth_address_URL": "https://etherscan.io/address/0xc72f78c9ebd0e843e61ec40d247f2cc504441fc3"
}

```